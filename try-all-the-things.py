#!/usr/bin/env python2

import argparse
import bz2
import codecs
import magic
import sys
import zlib

from pwn import *

# Make sure python does not choke on bytes outside of ASCII range
reload(sys)
sys.setdefaultencoding('utf8')

# Input option stuff
parser = argparse.ArgumentParser(
  description = 'Try a bunch of decoding and deobfuscation strategies.',
  epilog      = 'Takes piped input only.'
)
parser.add_argument('-r', '--recursive', help='Recurse output until search string is found.',                                action='store_true')
parser.add_argument('--strip',           help='Strip any newlines from input.',                                              action='store_true')
parser.add_argument('--search',          help='Search for specific substring in the output.',                                type=str)
parser.add_argument('--xor-size',        help='Maximum size of XOR key to brute force (default = 2 bytes)',                  type=int, default=2)
parser.add_argument('--no-xor',          help='Disable XOR brute force entirely. You probably want this in recursive mode.', action='store_true')
args   = parser.parse_args()
found  = False
inputs = []
depth  = 0
magic  = magic.Magic()

if args.recursive and not args.search:
  log.critical('Must specify a search string when using recursive mode.')
  sys.exit()

# Helper functions to generate keys for brute force
# God help you if you want to do more than 3 or 4 bytes.
def makeprintable(inp):
  outp = []
  for c in inp:
    if isprint(c):
      outp.append(c)
    else:
      outp.append('\\x' + enhex(c))
  return ''.join(outp)

def genbytes():
  all_bytes = []
  for byte in range(0,256):
    all_bytes.append(chr(byte))
  return all_bytes

def generator(max_size):
  keys = genbytes()

  size = 1
  while size < max_size:
    size += 1
    new_keys = []
    for k in keys:
      for b in genbytes():
        new_keys.append(''.join([k,b]))

    keys.extend(new_keys)

  return keys

# Try the input as the output
def nop_encode(i):
  result('NOP', i)
  # No return since nop does not need to be re-added

# See what libmagic has to say
def lib_magic(i):
  mimetype = magic.from_buffer(i)
  result('Libmagic suggests', mimetype)

# Try some encoding methods
def encoding(i):
  r = []

  try:
    tmp = b64d(i)
    r.append(tmp)
    result('Base 64 decode', tmp)
  except TypeError:
    # Turns out it's not base 64.
    pass

  try:
    tmp = bitswap(i)
    r.append(tmp)
    result('Bitswap', tmp)
  except ValueError:
    # Something has gone wrong with input encoding
    pass

  try:
    tmp = unhex(i)
    r.append(tmp)
    result('Unhex', tmp)
  except TypeError:
    # Turns out it's not hex encoded.
    pass

  try:
    tmp = rol(i, 1)
    r.append(tmp)
    result('Rotate left', tmp)
  except ZeroDivisionError:
    pass

  try:
    tmp = ror(i, 1)
    r.append(tmp)
    result('Rotate right', tmp)
  except ZeroDivisionError:
    pass

  try:
    tmp = urldecode(i)
    r.append(tmp)
    result('URL decode', tmp)
  except ValueError:
    # Some invalid input that looks like urlencoded input.
    pass

  try:
    tmp = codecs.decode(i, 'rot_13')
    r.append(tmp)
    result('Rot 13', tmp)
  except UnicodeDecodeError:
    # Not a suitable input range
    pass

  try:
    tmp = codecs.decode(i, 'punycode')
    r.append(tmp)
    result('Punycode', tmp)
  except IndexError:
    # Cannot decode as punycode
    pass
  except ValueError:
    # Some other reason for not being able to decode as punycode
    pass
  except UnicodeError:
    # Invalid unicode input
    pass

  try:
    tmp = codecs.decode(i, 'idna')
    r.append(tmp)
    result('idna', tmp)
  except IndexError:
    # Cannot decode as idna
    pass
  except ValueError:
    # Some other reason for not being able to decode as idna
    pass
  except UnicodeError:
    # Invalid unicode input
    pass

  tmp = codecs.decode(i, 'quopri_codec')
  r.append(tmp)
  result('Quoted printable', tmp)

  try:
    tmp = codecs.decode(i, 'uu_codec')
    r.append(tmp)
    result('Uu encoding', tmp)
  except ValueError:
    # Does not appear to be uu encoded
    pass

  try:
    tmp = codecs.decode(i, 'string_escape')
    r.append(tmp)
    result('Unescape string', tmp)
  except ValueError:
    # Some decoding error
    pass

  return r

# Try some compression methods
def compression(i):
  r = []

  try:
    tmp = zlib.decompress(i)
    r.append(tmp)
    result('zlib decompression', tmp)
  except zlib.error:
    # Not zlib compressed
    pass

  try:
    tmp = bz2.decompress(i)
    r.append(tmp)
    result('bzip2 decompression', tmp)
  except IOError:
    # Not bz2 compressed
    pass

  return r

# Try to do XOR brute force
def xor_brute(i):
  r = []
  for key in generator(args.xor_size):
    tmp = xor(i, key)
    r.append(tmp)
    result('XOR with key "' + makeprintable(key) + '"', tmp)
  return r

# Process all of the things
def process():
  global inputs
  for i in inputs:
    if args.strip:
      i = i.replace('\n', '')

    nop_encode(i)
    lib_magic(i)
    next_inputs = []
    next_inputs.extend(encoding(i))
    next_inputs.extend(compression(i))
    if not args.no_xor:
      next_inputs.extend(xor_brute(i))

    # Copy a de-duplicated set for recursive usage
    inputs = list(set(next_inputs))

# Helper function for showing output
def result(type, output):
  if args.search:
    if output.find(args.search) >= 0:
      global found
      found = True
      log.success('Found matching output for ' + type + ':')
      log.success(makeprintable(output))
      print ''
  else:
    log.info(type + ':')
    log.info(makeprintable(output))
    print ''

# Script entry point
if __name__== "__main__":
  inputs = [sys.stdin.read()]
  process()

  if args.recursive and not found:
    rec = log.progress('Recursion depth')
    while not found:
      depth = depth + 1
      rec.status(str(depth))
      process()
